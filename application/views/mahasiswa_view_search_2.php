<!DOCTYPE html>

<html>
<head>
	<title>Data Mahasiswa</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
	<style>
		body{
			background-image:url('<?php echo base_url().'assets/images/main-bg.jpg' ;?>');
			background-repeat-x: no-repeat;
		}
	</style>
</head>

<body>
	<!--Header -->
	<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
		<a class="navbar-brand" href="#">Dharmavira <?php echo $angkatan	 ?></a>
	</nav>

	<!--Isi-->		
	<div class="container">
		<div class="row">
			<h2><small> Data Mahasiswa</small></h2>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr class="danger"> 
							<th>NPM</th>
							<th>NAMA</th>
							<th>PRODI</th>
							<th>QR CODE</th>
							<th>ACTION</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($data->result() as $row):?>
						<tr class="success">
							<td style="vertical-align: middle;"><?php echo $row->npm;?></td>
							<td style="vertical-align: middle;"><?php echo $row->nama;?></td>
							<td style="vertical-align: middle;"><?php echo $row->prodi;?></td>
							<td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$row->qr_code;?>"></td>
							<td style="vertical-align: middle;">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Show</button>
								
								<a href="<?php echo base_url().'index.php/mahasiswa/hapus/'.$row->npm?>" class="btn btn-danger" role="button">Delete</a>
							</td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Detail Mahasiswa</h4>
			</div>
			<div class="modal-body">
				<h4>NPM : <?php echo $row->npm;?></h4>
				<h4>Nama : <?php echo $row->nama;?></h4>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			</div>
		</div>
	</div>

	
	<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-2.1.4.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
</body>
</html>