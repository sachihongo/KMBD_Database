<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo base_url().'assets/ico/kmbicon.png'?>"/>
<title>Data Mahasiswa KMBD</title>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/mahasiswa_view.css'?>">

<script type="text/javascript">

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});
</script>
</head>

<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-4">
						<h2>Angkatan <b><?php echo $angkatan   ?></b></h2>
					</div>
					<div class="col-sm-8">						
						<a href="" class="btn btn-primary"><i class="material-icons">&#xE863;</i> <span>Refresh List</span></a>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th> </th>
                        <th>Nama</th>
						<th>Prodi</th>
						<th>Order Date</th>			
                        <th>Status</th>					
						<th>Net Amount</th>
						<th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php 
                    $i=0;
                    foreach($data->result() as $row):
                    $i++; 
                    ?>   
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><img src="<?php echo base_url().'assets/images/sachi.png'?>" class="img-circle" alt="Cinque Terre" width="90" height=90"></td>
                        <td><a href="#"><?php echo $row->nama;?></a></td>
						<td><?php echo $row->prodi;?></td>
                        <td>Jun 15, 2017</td>                        
						<td><span class="status text-success">&bull;</span> Verified</td>
						<td>$254</td>
						<td><a href="<?php echo base_url().'index.php/mahasiswa/lihat/'.$row->npm?>" class="view" title="View Details" data-toggle="tooltip"><i class="material-icons">&#xE5C8;</i></a></td>
                    </tr>
                    <?php endforeach;?>
                    
                    
                </tbody>
            </table>
			<div class="clearfix">
                <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                <ul class="pagination">
                    <!--Tampilkan pagination-->
                    <?php echo $pagination; ?>
                </ul>
            </div>
            
        </div>
    </div>     
</body>

</html>                                		                            