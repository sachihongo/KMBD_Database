﻿<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/ico/kmbicon.png'?>"/>
    <title>Detail Mahasiswa </title>
    <!-- BOOTSTRAP STYLE SHEET -->
    <link href="<?php echo base_url().'assets/css/bootstrap.css';?>" rel="stylesheet" />
    <!-- FONT AWESOME ICONS STYLE SHEET -->
    <link href="<?php echo base_url().'assets/css/font-awesome.css';?>" rel="stylesheet" />
    <!-- FANCYBOX POPUP CSS STYLES -->
    <link href="<?php echo base_url().'assets/js/source/jquery.fancybox.css';?>" rel="stylesheet" />
    <!-- CUSTOM STYLES -->
    <link href="<?php echo base_url().'assets/css/style_view_detail.css';?>" rel="stylesheet" />
    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <a class="navbar-brand" href="index.html">KMBD Unpad</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url().'index.php/mahasiswa/masuk'?>">Log In</a></li>
          <li><a href="<?php echo base_url().'index.php/mahasiswa/keluar'?>">Log Out</a></li>
        </ul>
      </div>
      <!--/.nav-collapse -->
        </div>
    </div>

    <div class="container">
        <div class="row">
            <?php 
                    $i=0;
                    foreach($data->result() as $row):
                    $i++; 
                    ?> 
            <div class="col-md-3">

                <div class="user-wrapper">
                    <img src="<?php echo base_url().'assets/images/sachi.png';?>" class="img-responsive" />
                    <div class="description">
                        <h4 class="set-clr"><strong><?php echo $row->nama; ?> </strong></h4>
                        <h5><strong><?php echo $row->npm; ?></strong></h5>
                        <hr />
                        <a href="#" class="btn btn-info"><i class="fa fa-user-plus"></i>&nbsp;Download Resume </a>
                        <hr />
                        <a href="#" class="btn btn-social btn-facebook">
                            <i class="fa fa-facebook"></i></a>

                        <a href="#" class="btn btn-social btn-google">
                            <i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-social btn-twitter">
                            <i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-social btn-linkedin">
                            <i class="fa fa-linkedin"></i></a>
                        <hr />
                    </div>
                </div>
                <!--USER WRAPPER SECTION END-->
            </div>
            <!--LEFT SIDE SECTION END-->
            <div class="col-md-9  user-wrapper">
                <div class="description">
                    <!--WHO AM I SECTION END-->
                    
                        <div class="col-md-12">
                            <h3>PRODI</h3>
                            <p>
                                <?php echo $row->prodi; ?>
                            </p>
                        </div>
                    
                    <!--EDUCATION SECTION END-->
                    
                        <div class="col-md-12">
                            <h3>FAKULTAS</h3>
                            <p>
                                <?php echo $row->fakultas; ?>
                            </p>
                        </div>
                    
                    <!--EXPERIENCE SECTION END-->
                    
                        <div class="col-md-12">
                            <h3>ANGKATAN</h3>
                            <p>
                                <?php echo $row->angkatan; ?>
                            </p>
                        </div>
        
                    <!--SKILL SECTION END-->
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Recent Projects : </h3>
                            <p>
                                <img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$row->qr_code;?>">
                            </p>
                        </div>
                    </div>
                    <!--PROJECTS SECTION END-->
                    <hr />
                </div>
            </div>
            <!--RIGHT SIDE SECTION END-->
             <?php endforeach;?>
        </div>
    </div>

    <div class ="footer">
     <div class="container">
      <div class="row">
      
           © 2018 KMB Dharmavira Unpad  | <a href="" target="_blank">Developed by Sachi Hongo
        
        </div>
      </div>
     </div>
    </div>
    

</body>

</html>
