<!DOCTYPE html>
<html>
<head>
	<title>Data Mahasiswa</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
	<style>
	  .file {
	    visibility: hidden;
	    position: absolute;
	  }
 	</style>
</head>

<body>
	<div class="container">
		<div class="row">
			<h2>Data <small>Mahasiswa</small></h2>
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add New</button>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>NPM</th>
						<th>NAMA</th>
						<th>PRODI</th>
						<th>QR CODE</th>
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($data->result() as $row):?>
					<tr>
						<td style="vertical-align: middle;"><?php echo $row->npm;?></td>
						<td style="vertical-align: middle;"><?php echo $row->nama;?></td>
						<td style="vertical-align: middle;"><?php echo $row->prodi;?></td>
						<td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$row->qr_code;?>"></td>
						<td style="vertical-align: middle;">
						  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModaldelete">Show</button>
							<a href="<?php echo base_url().'index.php/mahasiswa/hapus/'.$row->npm?>" class="btn btn-danger" role="button">Delete</a>
						</td>
					</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal add new mahasiswa-->
	<form action="<?php echo base_url().'index.php/mahasiswa/simpan'?>" enctype="multipart/form-data" method="post">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Add New Mahasiswa</h4>
		      </div>
		      <div class="modal-body">
		    
		          <div class="form-group">
		            <label for="npm" class="control-label">NPM:</label>
		            <input type="text" name="npm" class="form-control" id="npm" required> 
		          </div>
		          <div class="form-group">
		            <label for="nama" class="control-label">NAMA:</label>
		            <input type="text" name="nama" class="form-control" id="nama" required>
		          </div>
							<div class="form-group">
		            <label for="prodi" class="control-label">PRODI:</label>
		            <input type="text" name="prodi" class="form-control" id="prodi" required>
		          </div>
	       		  <div class="form-group">
		            <label for="fakultas" class="control-label">FAKULTAS:</label>
		            <select name="fakultas" class="form-control" id="fakultas">
		            	<option>FMIPA</option>
		            	<option>FKG</option>
		            	<option>FTIP</option>
									<option>FH</option>
									<option>FISIP</option>
		            </select>
							</div>
							<div class="form-group">
		            <label for="angkatan" class="control-label">ANGKATAN:</label>
		            <select name="angkatan" class="form-control" id="angkatan">
		            	<option>2017</option>
		            	<option>2016</option>
		            	<option>2015</option>
									<option>2014</option>
									<option>2013</option>
									<option>2012</option>
		            </select>
		          </div>
				  <div class="form-group">
		            <label for="email" class="control-label">EMAIL:</label>
		            <input type="email" name="email" class="form-control" id="email">
		          </div>
		          <div class="form-group">
				      <label for="userfile">Gambar :</label>
				      <input type="file" name="userfile" class="file">
				      <div class="input-group col-xs-12">
				        <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
				        <input type="text" class="form-control input-lg" disabled placeholder="Upload Gambar">
				        <span class="input-group-btn">
				          <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Telusuri</button>
				        </span>
				      </div><br>
				   </div>	

	        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
		        <button type="submit" name="submit" class="btn btn-primary" >Simpan</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>

	<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-2.1.4.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
	<script type="text/javascript">
	    $(document).on('click', '.browse', function(){
	    var file = $(this).parent().parent().parent().find('.file');
	    file.trigger('click');
	    });
	    $(document).on('change', '.file', function(){
	    $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
	  });
  	</script>

</body>
</html>