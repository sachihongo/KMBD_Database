<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url().'assets/ico/kmbicon.png'?>"/>

  <title>Database KMBD</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap-theme.css'?>">

	<!-- siimple style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/style.css'?>">

  <!-- =======================================================
    Theme Name: Siimple
    Theme URL: https://bootstrapmade.com/free-bootstrap-landing-page/
    Author: SachiHongo
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>
  
<body>

  <!-- Fixed navbar -->
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <a class="navbar-brand" href="index.html">KMBD Unpad</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url().'index.php/mahasiswa/masuk'?>">Log In</a></li>
          <li><a href="<?php echo base_url().'index.php/mahasiswa/keluar'?>">Log Out</a></li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>

  <div id="header">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <h1>Database KMBD</h1>
          <h2 class="subtitle">Just choose the generaion of KMBD in the textbox below</h2>
          <form class="form-inline signup" action="<?php echo base_url().'index.php/mahasiswa/cari'?>" method="post" role="form">
            <div class="form-group">
                <label for="angkatan" class="control-label"></label>
		            <select name="angkatan" class="form-control" id="angkatan">
		            	<option>2017</option>
		            	<option>2016</option>
		            	<option>2015</option>
                  <option>2014</option>
		            </select>
            </div>
            <button type="submit" class="btn btn-theme">Search</button>
          </form>
        </div>
        <div class="col-lg-4 col-lg-offset-2">
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <!-- slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="<?php echo base_url().'assets/images/slide1.png'?>" alt="">
              </div>
              <div class="item">
                <img src="<?php echo base_url().'assets/images/slide2.png'?>" alt="">
              </div>
              <div class="item">
                <img src="<?php echo base_url().'assets/images/slide3.png'?>" alt="">
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <p class="copyright">&copy; 2018 Kmb Dharmavira Unpad</p>
        </div>
        <div class="col-md-6">
          <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Siimple
            -->
            <a href="https://bootstrapmade.com/">Made with Love</a> by <a href="https://bootstrapmade.com/">SachiHongo</a>
          </div>
        </div>
      </div>
    </div>
	</div>
	
	
  <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-2.1.4.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>

</body>

</html>
