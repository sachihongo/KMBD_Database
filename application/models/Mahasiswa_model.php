<?php
class Mahasiswa_model extends CI_Model{

	function get_all_mahasiswa(){
		$hasil=$this->db->get('mahasiswa');
		return $hasil;
	}

	public function get_mahasiswa($angkatan,$limit, $start)
    {
		$this->db->like('angkatan',$angkatan);
		$hasil=$this->db->get('mahasiswa',$limit, $start);
		return $hasil;
    }

    public function get_mahasiswa_detail($npm){
    	$this->db->like('npm',$npm);
    	$hasil=$this->db->get('mahasiswa');
    	return $hasil;
    }
	
	function simpan_mahasiswa($npm,$nama,$prodi,$fakultas,$tanggal_lahir,$angkatan,$email,$image_name,$image){	
		$data = array(
			'npm' 		    => $npm,
			'nama' 		    => $nama,
			'prodi' 	    => $prodi, 
			'fakultas'      => $fakultas,
            'tanggal_lahir' => $tanggal_lahir,
			'angkatan'      => $angkatan,
			'email'         => $email,
			'qr_code' 	    => $image_name,
			'gambar'        => $image
		);
		$this->db->insert('mahasiswa',$data);
	}

	public function update($post,$npm)
    {
        $nama=$this->db->escape($post['nama']);
        $alamat=$this->db->escape($post['alamat']);
        $email=$this->db->escape($post['email']);
        
        $sql=$this->db->query("UPDATE tb_karyawan SET name=$nama,alamat=$alamat,email=$email WHERE id = ".intval($id));
 
        return true;
    }

	function delete_mahasiswa($npm){
		$this->db->where('npm',$npm);
		$this->db->delete('mahasiswa');	
	}
	
}