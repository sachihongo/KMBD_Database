<?php 

class Mahasiswa extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('mahasiswa_model'); //pemanggilan model mahasiswa	
	}

	function index(){
		$this->load->view('home');
	}

	function page(){
		$data['data']=$this->mahasiswa_model->get_all_mahasiswa();
		$this->load->view('mahasiswa_view',$data);
	}

	function cari(){
		$this->load->library('pagination');
		
		$angkatan=$this->input->post('angkatan');
		//konfigurasi pagination
        $config['base_url'] = site_url('mahasiswa/cari'); //site url
        $config['total_rows'] = $this->db->count_all('mahasiswa'); //total row
        $config['per_page'] = 4;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
 
        // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 
		$data['angkatan']=$this->input->post('angkatan');
		$data['data']=$this->mahasiswa_model->get_mahasiswa($angkatan,$config["per_page"], $data['page']);
        $data['pagination'] = $this->pagination->create_links();
		$this->load->view('mahasiswa_view_search',$data);
	}

	function lihat($npm){
		$data['data']=$this->mahasiswa_model->get_mahasiswa_detail($npm);
		$this->load->view('mahasiswa_view_search_detail',$data);
	}

	function masuk(){
		$this->load->view('login.php');
	}
    
    function login(){
        $this->load->   model('login_model');
        $this->load->library('session');
		$this->data['error'] = '';	
        $username = $this->input->post('form-username');
		$password = $this->input->post('form-password');

		$cek = $this->login_model->cek($username, $password);
		if($cek->num_rows() == 1)
		{
			foreach($cek->result() as $data){
				$sess_data['id_user'] = $data->id_user;
				$sess_data['username'] = $data->username;
				$sess_data['id_jenis_user'] = $data->id_jenis_user;
				$this->session->set_userdata($sess_data);
			}

			if($this->session->userdata('id_jenis_user') == '1')
			{
				redirect('Admin');
			}
			elseif($this->session->userdata('id_jenis_user') == '2')
			{
				redirect('index.php/contributor');
			}
			elseif($this->session->userdata('id_jenis_user') == '0')
			{
				$this->load->view('Home_User');
			}
		}
		else
		{
			$this->session->set_flashdata('pesan', 'Maaf, kombinasi username dengan password salah.');
			redirect('index.php/Main_Controller');
		}
    }

	function keluar(){
		$this->load->view('mahasiswa_view_search_detail.php');
	}

    function logout(){
        $this->session->sess_destroy();
		redirect('mahasiswa');
    }
    
	function simpan(){
		$this->load->helper(array('form', 'url'));

		$npm=$this->input->post('npm');
		$this->load->library('ciqrcode'); //pemanggilan library QR CODE

		$config['cacheable']	= true; //boolean, the default is true
		$config['cachedir']		= './assets/'; //string, the default is application/cache/
		$config['errorlog']		= './assets/'; //string, the default is application/logs/
		$config['imagedir']		= './assets/images/'; //direktori penyimpanan qr code
		$config['quality']		= true; //boolean, the default is true
		$config['size']			= '1024'; //interger, the default is 1024
		$config['black']		= array(224,255,255); // array, default is array(255,255,255)
		$config['white']		= array(70,130,180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($config);

		$image_name=$npm.'.png'; //buat name dari qr code sesuai dengan nim

		$params['data'] = $npm; //data yang akan di jadikan QR CODE
		$params['level'] = 'H'; //H=High
		$params['size'] = 10;
		$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

		//membuat konfigurasi
	    $config['upload_path']="./assets/images/";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
	    $this->load->library('upload', $config);
	    $this->upload->initialize($config);

	    if($this->upload->do_upload("userfile")){
            $data = array('upload_data' => $this->upload->data());
 			
 			$npm=$this->input->post('npm');
			$nama=$this->input->post('nama');
			$prodi=$this->input->post('prodi');
			$fakultas=$this->input->post('fakultas');
            $tanggal_lahir=$this->input->post('tanggal_lahir');
			$angkatan=$this->input->post('angkatan');
			$email=$this->input->post('email');
            
            $image= $data['upload_data']['file_name']; 
             
            $this->mahasiswa_model->simpan_mahasiswa($npm,$nama,$prodi,$fakultas,$tanggal_lahir,$angkatan,$email,$image_name,$image); //simpan ke database
			redirect('admin/table'); //redirect ke mahasiswa usai simpan data
		
        }
        else{
        	echo "error in upload image file";
        }

	}

	public function edit($npm)
    {
        $data['default']=$this->mahasiswa_model->get_default($npm);
        if(isset($_POST['tombol_submit'])){
            $this->model_data->update($_POST,$id);
            redirect('Mahasiswa');
        }
        $this->load->view("mahasiswa_view",$data);
    }


	function hapus($npm){  
		$this->mahasiswa_model->delete_mahasiswa($npm);
		redirect('admin/table');
	}
    
}