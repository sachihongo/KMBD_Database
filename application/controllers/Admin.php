<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('id_jenis_user') <> '1')
		{
			redirect('mahasiswa');
		}
	}

	public function index(){
		$d['title'] = 'Login Multiuser Codeigniter dengan MySql &minus; AneIqbalcom';
		$d['judul'] = 'Login Multiuser Codeigniter dengan Mysql';
		$d['username'] = $this->session->userdata('username');
		$d['page'] = 'admin';
		$this->load->view('admin_view', $d);
	}
    
    function dashboard(){
        $d['username'] = $this->session->userdata('username');
		$d['page'] = 'admin';
        $this->load->view("admin_view", $d);
    }
    
    function table(){
        $this->load->model('mahasiswa_model');
        $d['username'] = $this->session->userdata('username');
		$d['page'] = 'admin';
        $d['data']=$this->mahasiswa_model->get_all_mahasiswa();
        $this->load->view("tables", $d);
    }
    
    function chart(){
        $d['username'] = $this->session->userdata('username');
        $d['page'] = 'admin';
        $this->load->view("charts", $d);
    }
    
    function input(){
        $d['username'] = $this->session->userdata('username');
        $d['page'] = 'admin';
        $this->load->view("input", $d);
    }
    
    function link(){
        $d['username'] = $this->session->userdata('username');
        $d['page'] = 'admin';
        $this->load->view("link", $d);
    }
	
}